# django-examples

Um projeto destinado a testes, exemplos e modelos para uso em produção.

## Introducão


***********************************

# Preparando o Sistema

Deixe seu OS atualizado:
```
$ sudo apt update && sudo apt full-upgrade -y
```
Criar ambiente virtual
```
$ python3 -m venv nome-do-ambiente-virtual
```
Usar ambiente virtual
```
$ source nome-do-ambiente-virtual/bin/activate
```

Atualizar pip do ambiente virtual
```
(venv) $ python3 -m pip install --upgrade pip
```
Instalando Django no ambiente virtual
```
(venv) $ pip install django
```
# Django

Iniciar um novo projeto djando
```
(venv) $ django-admin startproject nome-do-projeto .
```




Texto
```
texto
```
